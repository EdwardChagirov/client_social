import React from 'react'
import './App.css'
import { store, history } from 'store'
import { Router, Route } from 'react-router-dom'
import { Provider } from 'react-redux'

import Home from 'pages/Home'
import Authorization from 'pages/Authorization'

function App() {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Route exact path="/" component={Authorization} />
        <Route path="/home" component={Home} />
      </Router>
    </Provider>
  )
}

export default App
