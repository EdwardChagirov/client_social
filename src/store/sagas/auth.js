import { put, call, takeEvery } from 'redux-saga/effects'
import axios from 'axios'
import { push } from 'react-router-redux'
import types from 'store/constants/auth'
import * as authActions from 'store/actions/auth'

const authUser = payload => {
  return axios.post('http://localhost:3030/user/auth', payload)
}

const regUser = payload => {
  return axios.post('http://localhost:3030/user/reg', payload)
}

function* auth(action) {
  try {
    const user = yield call(authUser, action.payload)
    if (!user.data.errors) {
      yield put(authActions.authSuccess(user.data))
      yield put(push('/home'))
    } else {
      throw user.data.errors
    }
  } catch (err) {
    yield put(authActions.authFailure(err))
  }
}

function* reg(action) {
  try {
    const user = yield call(regUser, action.payload)
    if (!user.data.errors) {
      yield put(authActions.regSuccess(user.data))
    } else {
      throw user.data.errors
    }
  } catch (err) {
    yield put(authActions.regFailure(err))
  }
}

function* authSaga() {
  yield takeEvery(types.AUTH, auth)
  yield takeEvery(types.REG, reg)
}

export default authSaga
