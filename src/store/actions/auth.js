import types from 'store/constants/auth'

export const auth = data => ({
  type: types.AUTH,
  payload: data,
})

export const authSuccess = data => ({
  type: types.AUTH_SUCCESS,
  payload: data,
})

export const authFailure = error => ({
  type: types.AUTH_FAILURE,
  payload: error,
})

export const reg = data => ({
  type: types.REG,
  payload: data,
})

export const regSuccess = data => ({
  type: types.REG_SUCCESS,
  payload: data,
})

export const regFailure = error => ({
  type: types.REG_FAILURE,
  payload: error,
})
