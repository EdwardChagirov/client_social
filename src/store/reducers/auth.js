import types from 'store/constants/auth'

const initialState = {
  id: '',
  login: '',
  errors: undefined,
  isGetFetching: false,
}

export default (state = initialState, action) => {
  switch (action.type) {
    case types.AUTH:
      return {
        ...state,
        isGetFetching: false,
      }

    case types.AUTH_SUCCESS:
      return {
        ...state,
        id: action.payload.id,
        login: action.payload.login,
        isGetFetching: true,
        errors: undefined,
      }

    case types.AUTH_FAILURE:
      return {
        ...state,
        errors: action.payload,
        isGetFetching: true,
      }

    case types.REG:
      return {
        ...state,
        isGetFetching: false,
      }

    case types.REG_SUCCESS:
      return {
        ...state,
        id: action.payload.id,
        login: action.payload.login,
        isGetFetching: true,
        errors: undefined,
      }

    case types.REG_FAILURE:
      return {
        ...state,
        errors: action.payload,
        isGetFetching: true,
      }

    default:
      return state
  }
}
