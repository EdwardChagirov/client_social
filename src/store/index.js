import { createStore, applyMiddleware, combineReducers } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import createSagaMiddleware from 'redux-saga'
import { createBrowserHistory } from 'history'
import { routerMiddleware, routerReducer } from 'react-router-redux'

// Reducers
import authReducer from 'store/reducers/auth'

// Sagas
import authSaga from 'store/sagas/auth'

const history = createBrowserHistory()

const sagaMiddleware = createSagaMiddleware()

const middleware = [sagaMiddleware, routerMiddleware(history)]

const appReducer = combineReducers({
  auth: authReducer,
  routing: routerReducer,
})

const store = createStore(appReducer, composeWithDevTools(applyMiddleware(...middleware)))

sagaMiddleware.run(authSaga)

export { store, history }
