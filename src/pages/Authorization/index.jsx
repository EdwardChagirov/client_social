import React from 'react'
import Auth from 'containers/Auth'
import Reg from 'containers/Reg'
import styles from './Authorization.module.css'

const Authorization = () => (
  <div className={styles.authorization}>
    <Auth />
    <Reg />
  </div>
)

export default Authorization
