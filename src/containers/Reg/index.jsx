import React, { useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as authActions from 'store/actions/auth'
import PropTypes from 'prop-types'
import Reg from 'components/Reg'

const RegContainer = props => {
  const [userLogin, setUserLogin] = useState('')
  const [userPassword, setUserPassword] = useState('')

  const onSubmit = e => {
    e.preventDefault()

    const { authDispatch } = props
    const data = {
      login: userLogin,
      password: userPassword,
    }

    authDispatch.reg(data)
  }

  const { errors } = props

  return <Reg onSubmit={onSubmit} setUserLogin={setUserLogin} setUserPassword={setUserPassword} errors={errors} />
}

const mapStateToProps = state => ({
  errors: state.auth.errors,
})

const mapDispatchToProps = dispatch => ({
  authDispatch: bindActionCreators(authActions, dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(RegContainer)

RegContainer.propTypes = {
  authDispatch: PropTypes.object.isRequired,
  errors: PropTypes.array,
}

RegContainer.defaultProps = {
  errors: undefined,
}
