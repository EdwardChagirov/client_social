import React, { useState } from 'react'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as authActions from 'store/actions/auth'
import PropTypes from 'prop-types'
import Auth from 'components/Auth'

const AuthContainer = props => {
  const [userLogin, setUserLogin] = useState('')
  const [userPassword, setUserPassword] = useState('')

  const onSubmit = e => {
    e.preventDefault()

    const { authDispatch } = props
    const data = {
      login: userLogin,
      password: userPassword,
    }

    authDispatch.auth(data)
  }

  const { errors } = props

  return (
    <>
      <Auth onSubmit={onSubmit} setUserLogin={setUserLogin} setUserPassword={setUserPassword} errors={errors} />
    </>
  )
}

const mapStateToProps = state => ({
  errors: state.auth.errors,
})

const mapDispatchToProps = dispatch => ({
  authDispatch: bindActionCreators(authActions, dispatch),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AuthContainer)

AuthContainer.propTypes = {
  authDispatch: PropTypes.objectOf(PropTypes.func).isRequired,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string,
      path: PropTypes.string,
    })
  ),
}

AuthContainer.defaultProps = {
  errors: undefined,
}
