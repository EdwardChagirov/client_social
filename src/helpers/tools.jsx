import _ from 'lodash'
import React from 'react'
import styles from './Helpers.module.css'

const renderErrors = (errors, path) => {
  const filteredErrors = _.filter(errors, error => error.path === path)

  return (
    !_.isEmpty(filteredErrors) && (
      <ul className={styles.errors}>
        {_.map(filteredErrors, (error, index) => (
          <li key={error.id} className={styles.error}>
            <span>{`${index + 1}. `}</span>
            <span>{error.message}</span>
          </li>
        ))}
      </ul>
    )
  )
}

export default renderErrors
