import React from 'react'
import PropTypes from 'prop-types'
import renderErrors from 'helpers/tools'
import styles from './Reg.module.css'

const Reg = ({ onSubmit, setUserLogin, setUserPassword, errors }) => (
  <div>
    <form className={styles.form} onSubmit={onSubmit}>
      <div className={styles.control}>
        <input
          className={styles.input}
          type="input"
          onChange={e => setUserLogin(e.target.value)}
          placeholder="Ваш логин"
        />
        {renderErrors(errors, 'login', styles.error)}
      </div>

      <div className={styles.control}>
        <input
          className={styles.input}
          type="input"
          onChange={e => setUserPassword(e.target.value)}
          placeholder="Ваш пароль"
        />
        {renderErrors(errors, 'password', styles.error)}
      </div>
      <button className={styles.button} type="submit">
        Зарегистрироваться
      </button>
    </form>
  </div>
)

export default Reg

Reg.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  setUserLogin: PropTypes.func.isRequired,
  setUserPassword: PropTypes.func.isRequired,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string,
      path: PropTypes.string,
    })
  ),
}

Reg.defaultProps = {
  errors: undefined,
}
