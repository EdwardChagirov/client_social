import React from 'react'
import PropTypes from 'prop-types'
import renderErrors from 'helpers/tools'
import styles from './Auth.module.css'

const Auth = ({ onSubmit, setUserLogin, setUserPassword, errors }) => {
  return (
    <div className={styles.component}>
      <form className={styles.form} onSubmit={onSubmit}>
        <input
          className={styles.input}
          type="input"
          onChange={e => setUserLogin(e.target.value)}
          placeholder="Ваш логин"
        />
        <input
          className={styles.input}
          type="input"
          onChange={e => setUserPassword(e.target.value)}
          placeholder="Ваш пароль"
        />
        <button className={styles.button} type="submit">
          Авторизоваться
        </button>
      </form>
      {renderErrors(errors, 'auth')}
    </div>
  )
}

export default Auth

Auth.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  setUserLogin: PropTypes.func.isRequired,
  setUserPassword: PropTypes.func.isRequired,
  errors: PropTypes.arrayOf(
    PropTypes.shape({
      message: PropTypes.string,
      path: PropTypes.string,
    })
  ),
}

Auth.defaultProps = {
  errors: undefined,
}
